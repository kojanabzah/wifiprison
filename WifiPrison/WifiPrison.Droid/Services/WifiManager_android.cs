using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Net;
using Android.Net.Wifi;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WifiPrison.Droid.Services;
using WifiPrison.Interfaces;
using WifiPrison.Models;


[assembly: Xamarin.Forms.Dependency(typeof(WifiManager_android))]
namespace WifiPrison.Droid.Services
{
    public class WifiManager_android : IWifiManager
    {
     
        private ConnectivityManager connectivityManager;

        private static WifiManager wifi;
        private WifiReceiver wifiReceiver;
        public static List<WifiItem> WiFiNetworks;

        private EventHandler _callback;


        public WifiManager_android()
        {
            connectivityManager = (ConnectivityManager)Application.Context.GetSystemService(Context.ConnectivityService);
        
        }
        public bool IsWifiConnected()
        {
            var wifiInfo = connectivityManager.GetNetworkInfo(ConnectivityType.Wifi);


            return wifiInfo.IsConnected;
        }

       

        public bool IsRoaming()
        {
            var wifiInfo = connectivityManager.GetNetworkInfo(ConnectivityType.Mobile);


            return wifiInfo.IsConnected;
        }

        public string GetWifiExtraInfo()
        {
            var wifiInfo = connectivityManager.GetNetworkInfo(ConnectivityType.Wifi);


            return wifiInfo.ExtraInfo;
        }

        public bool IsWifiEnabled()
        {
            var wifiManager = (WifiManager)Application.Context.GetSystemService(Context.WifiService);

            return wifiManager.IsWifiEnabled;
        }

        public void ConnectWifi(string networkSSID,string password)
        {
            var wifiConfig = new WifiConfiguration();
            wifiConfig.Ssid = string.Format("\"{0}\"", networkSSID);
            wifiConfig.PreSharedKey = string.Format("\"{0}\"", password);

            var wifiManager = (WifiManager)Application.Context.GetSystemService(Context.WifiService);

           

            // Use ID
            var netId = wifiManager.AddNetwork(wifiConfig);
            
            wifiManager.Disconnect();
            wifiManager.EnableNetwork(netId, true);
            wifiManager.Reconnect();
        }

        public void GetWifiLevel(EventHandler callback)
        {
         
        }

        public string GetConnectionType()
        {
            var activeConnection = connectivityManager.ActiveNetworkInfo;
            return activeConnection.TypeName;

        }

        public void GetWifiNetworks(EventHandler callback)
        {
            WiFiNetworks = new List<WifiItem>();
        
            try
            {
                // Get a handle to the Wifi
                wifi = (WifiManager) Application.Context.GetSystemService(Context.WifiService);

                // Start a scan and register the Broadcast receiver to get the list of Wifi Networks
                wifiReceiver = new WifiReceiver(callback);
                Application.Context.RegisterReceiver(wifiReceiver,
                    new IntentFilter(WifiManager.ScanResultsAvailableAction));
                wifi.StartScan();
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

        }

        public List<WifiItem> GetWifiList()
        {
            return WiFiNetworks;
        }


        class WifiReceiver : BroadcastReceiver
        {
            public event EventHandler ConnectionStatusChanged;
            public WifiReceiver(EventHandler cb)
            {
                ConnectionStatusChanged = cb;
            }
            
            public override void OnReceive(Context context, Intent intent)
            {
                WiFiNetworks = new List<WifiItem>();

                var scanwifinetworks = wifi.ScanResults;
                foreach (ScanResult wifinetwork in scanwifinetworks)
                {
                    if (!WiFiNetworks.Any(x=>x.Name == wifinetwork.Ssid) && !string.IsNullOrEmpty(wifinetwork.Ssid))
                    {

                        var item = new WifiItem();
                        item.Name = wifinetwork.Ssid;
                        item.Level = wifinetwork.Level;
                        item.SignalLevel = WifiManager.CalculateSignalLevel(item.Level, 100) + "%";
                        item.Frequency = wifinetwork.Frequency;
                        item.Capabilities = wifinetwork.Capabilities;
                        WiFiNetworks.Add(item);
                    }

                }

                if (ConnectionStatusChanged != null)
                    ConnectionStatusChanged(this, EventArgs.Empty);
            }

        }




    }


}