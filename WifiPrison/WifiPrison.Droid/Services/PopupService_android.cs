using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using WifiPrison.Droid.Services;
using WifiPrison.Interfaces;
using Exception = Java.Lang.Exception;

[assembly: Xamarin.Forms.Dependency(typeof(PopupService_android))]

namespace WifiPrison.Droid.Services
{
    public class PopupService_android :IPopupService
    {
        public async Task ShowDialog()
        {
            try
            {
                AlertDialog.Builder alert = new AlertDialog.Builder(Application.Context);

                alert.SetTitle("Hi, how are you");

                alert.SetPositiveButton("Good", (senderAlert, args) =>
                {
                    //change value write your own set of instructions
                    //you can also create an event for the same in xamarin
                    //instead of writing things here
                });

                alert.SetNegativeButton("Not doing great", (senderAlert, args) =>
                {
                    //perform your own task for this conditional button click
                });
                //run the alert in UI thread to display in the screen

                alert.Show();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}