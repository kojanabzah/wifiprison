﻿
using WifiPrison.ViewModels;

using Xamarin.Forms;

namespace WifiPrison.Views
{
    public partial class MainPage : ContentPage
    {
        private int FixTwice = 0;
        public MainPage()
        {
            InitializeComponent();
           
        }

        


        private void ListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            //"Info","Wifi List","History","Settings"
            if (FixTwice == 0)
            {
                FixTwice++;
                var dc = this.BindingContext as MainPageViewModel;
                if (dc != null)
                {
                    var list = (ListView)sender;
                    switch (list.SelectedItem as string)
                    {
                        case "Info":
                            dc.NavigateToWifi();
                            break;
                        case "Wifi List":
                            dc.NavigateToWifiList();
                            break;
                        case "History":
                            dc.NavigateToWHistory();
                            break;
                        case "Settings":
                            dc.NavigateToSettings();
                            break;

                    }
                    

                list.SelectedItem = null;
                }

            }
            else if (FixTwice != 0) FixTwice = 0;

        }
    }
}
