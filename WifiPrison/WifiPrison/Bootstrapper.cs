﻿using WifiPrison.Views;
using Microsoft.Practices.Unity;
using Prism.Unity;
using WifiPrison.ViewModels;
using Xamarin.Forms;
using WifiPrison.Views;

namespace WifiPrison
{
    public class Bootstrapper : UnityBootstrapper
    {
        protected override Page CreateMainPage()
        {
            return Container.Resolve<MainPage>();
        }

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<MainPage,MainPageViewModel>();
            Container.RegisterTypeForNavigation<WifiPage,WifiPageViewModel>();
            Container.RegisterTypeForNavigation<WifiList, WifiListViewModel>();
            Container.RegisterTypeForNavigation<HistoryPage, HistoryPageViewModel>();
            Container.RegisterTypeForNavigation<SettingsPage, SettingsPageViewModel>();
            Container.RegisterTypeForNavigation<ConnectPage, ConnectPageViewModel>();

        }
    }
}
