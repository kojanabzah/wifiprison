﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using WifiPrison.Interfaces;
using WifiPrison.Models;
using WifiPrison.Services;
using Xamarin.Forms;

namespace WifiPrison.Data
{
    public class WifiItemDatabase
    {
        static object locker = new object();
        SQLiteConnection database;
        public WifiItemDatabase()
        {
            try
            {
                database = DependencyService.Get<ISQLite>().GetConnection();
                // create the tables
                database.CreateTable<WifiDbItem>();
            }
            catch (Exception ex)
            {
                if (ex != null) { }
            }
        }

        public IEnumerable<WifiDbItem> GetItems()
        {
            lock (locker)
            {
                return (from i in database.Table<WifiDbItem>() select i).ToList();
            }
        }

        public IEnumerable<WifiDbItem> GetItemsNotDone()
        {
            lock (locker)
            {
                return database.Query<WifiDbItem>("SELECT * FROM [WifiDbItem] WHERE [Done] = 0");
            }
        }

        public WifiDbItem GetItem(string name)
        {
            lock (locker)
            {
                return database.Table<WifiDbItem>().FirstOrDefault(x => x.Name == name);
            }
        }

        public int SaveItem(WifiDbItem item)
        {
            lock (locker)
            {
                var check = GetItem(item.Name);
                if (check != null)
                {
                    item.ID = check.ID;
                    database.Update(item);
                    return 0;
                }
                else {
                    return database.Insert(item);
                }
            }
        }

        public int DeleteItem(int id)
        {
            lock (locker)
            {
                return database.Delete<WifiDbItem>(id);
            }
        }
    }
}
