﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Prism.Logging;
using WifiPrison.Interfaces;
using WifiPrison.Services;

[assembly: Xamarin.Forms.Dependency(typeof(Logger))]
namespace WifiPrison.Services
{
    public class Logger : ILooger
    {
        private readonly DebugLogger _logger;
        public Logger()
        {
            _logger = new DebugLogger();
        }

        public void Info(string msg)
        {
            _logger.Log(msg,Category.Info, Priority.None);
        }

        
    }
}
