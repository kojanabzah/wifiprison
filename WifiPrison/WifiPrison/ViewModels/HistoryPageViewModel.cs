﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;
using Prism.Navigation;
using WifiPrison.Models;

namespace WifiPrison.ViewModels
{
    public class HistoryPageViewModel : BindableBase, INavigationAware
    {
        private readonly INavigationService _navigationService;
        private List<WifiDbItem> _dbList;

        public List<WifiDbItem> DbList
        {
            get { return _dbList; }
            set { SetProperty(ref _dbList, value); }
        }

        public HistoryPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            DbList = App.Database.GetItems().ToList();



        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
           
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            DbList = App.Database.GetItems().ToList();
        }
    }
}
