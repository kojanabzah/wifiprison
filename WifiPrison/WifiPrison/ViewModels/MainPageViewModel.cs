﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Logging;
using Prism.Navigation;
using Prism.Services;
using WifiPrison.Base;
using WifiPrison.Interfaces;
using WifiPrison.Views;
using Xamarin.Forms;

namespace WifiPrison.ViewModels
{
    public class MainPageViewModel : CustomViewModel
    {
        public DelegateCommand GetWifiButton { get; set; }
        public DelegateCommand WifiListButton { get; set; }
        private readonly INavigationService _navigationService;
        private readonly IPageDialogService _dialogService;
        private readonly IWifiManager _wifiManager;


        private string _mainText = "test";

        private List<string> _menuItems = new List<string>() {"Info","Wifi List","History"};

        public List<string> MenuItems
        {
            get { return _menuItems; }
            set { SetProperty(ref _menuItems, value); }
        }




        public string MainText
        {
            get { return _mainText; }
            set { SetProperty(ref _mainText, value); }
        }

        private ILooger _looger;

        public MainPageViewModel(ILooger looger, INavigationService navigationService, IPageDialogService dialogService, IWifiManager wifiManager)
        {
            _looger = looger;

            _looger.Info("MainPageViewModel-ctor");

            _navigationService = navigationService;
            _dialogService = dialogService;
            _wifiManager = wifiManager;

            GetWifiButton = new DelegateCommand(NavigateToWifi);
            WifiListButton = new DelegateCommand(NavigateToWifiList);
        }


        public  void NavigateToWifi()
        {
            _navigationService.Navigate< WifiPageViewModel>();
        }

        public void NavigateToWifiList()
        {
            if (_wifiManager.IsWifiEnabled())
            {
                _navigationService.Navigate<WifiListViewModel>();
            }
            else
            {

                _dialogService.DisplayAlert("Error", "Your WIFI is not enabled, please enable it.", "Close");
            }
        }

        public void NavigateToSettings()
        {
            _navigationService.Navigate<SettingsPageViewModel>();
        }

        public void NavigateToWHistory()
        {
            _navigationService.Navigate<HistoryPageViewModel>();
        }



    }
}
