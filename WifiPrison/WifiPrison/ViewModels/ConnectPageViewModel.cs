﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using WifiPrison.Interfaces;
using WifiPrison.Models;

namespace WifiPrison.ViewModels
{
    public class ConnectPageViewModel : BindableBase, INavigationAware
    {
        private readonly INavigationService _navigationService;
        private readonly IWifiManager _wifiManager;
        private readonly IPageDialogService _dialogService;

        public DelegateCommand ConnectCommand { get; set; }

        private string _password;

        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }


        private WifiItem _selected = new WifiItem();

        public WifiItem Selected
        {
            get { return _selected; }
            set { SetProperty(ref _selected, value); }
        }



        public ConnectPageViewModel(IWifiManager wifiManager, INavigationService navigationService, IPageDialogService dialogService)
        {
            _navigationService = navigationService;
            _wifiManager = wifiManager;
            _dialogService = dialogService;

            ConnectCommand = new DelegateCommand(Connect);


        }



        public void OnNavigatedFrom(NavigationParameters parameters)
        {

        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            try
            {
                Selected = (WifiItem) parameters["selected"];
                var getSaved = App.Database.GetItem(Selected.Name);
                if (getSaved!=null)
                {
                    Password = getSaved.Password;
                }
            }
            catch (Exception ex)
            {
                if (ex != null) { }
            }
        }
    
        private void Connect()
        {
            if (_wifiManager.IsWifiEnabled())
            {
                _wifiManager.ConnectWifi(Selected.Name, Password);
                App.Database.SaveItem(new WifiDbItem()
                {
                    Name = Selected.Name,
                    Password = Password,
                    TimeConnected = DateTime.Now
                });

                _navigationService.Navigate<MainPageViewModel>();
            }
            else
            {
                _dialogService.DisplayAlert("Connect", "Your WIFI is not enabled", "ok");
            }
        }
    }
}
