﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using WifiPrison.Base;
using WifiPrison.Interfaces;
using WifiPrison.Models;
using XLabs.Forms.Controls;
using WifiPrison.Services;
using WifiPrison.Views;
using Xamarin.Forms;

namespace WifiPrison.ViewModels
{
    public class WifiListViewModel : CustomViewModel
    {
        private readonly INavigationService _navigationService;
        private readonly IWifiManager _wifiManager;
        private readonly IPageDialogService _dialogService;
        private readonly IPopupService _popUpService;

        private List<WifiItem> _wifiList;

        public List<WifiItem> WifiList
        {
            get { return _wifiList; }
            set { SetProperty(ref _wifiList, value); }
        }

        private WifiItem _selectedItem;

        public WifiItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (_selectedItem != value)
                {
                    _selectedItem = value;
                    ListSelectionChanged();
                }
                
                
            }
        }

 

        public WifiListViewModel(INavigationService navigationService, IWifiManager wifiManager, IPageDialogService dialogService, IPopupService popUpService)
        {
            _wifiManager = wifiManager;
            _navigationService = navigationService;
            _dialogService = dialogService;
            _popUpService = popUpService;
            InitWifi();

        
        }

        private async void ListSelectionChanged()
        {


            var parameters = new NavigationParameters();
            parameters.Add("selected", SelectedItem);

            _navigationService.Navigate<ConnectPageViewModel>(parameters);

            /*var answer = await _dialogService.DisplayAlert(SelectedItem.Name, string.Format("Level: {0} \nFrequency: {1}", SelectedItem.Level, SelectedItem.Frequency), "Connect","Close");
              if (answer)
              {


                   _wifiManager.ConnectWifi(SelectedItem.Name);
              }*/


        }

        private void InitWifi()
        {
            _wifiManager.GetWifiNetworks(UpdateWifiList);
        }

        private void UpdateWifiList(object sender, EventArgs eventArgs)
        {
            WifiList = _wifiManager.GetWifiList();
            
        }

        


    }
}
