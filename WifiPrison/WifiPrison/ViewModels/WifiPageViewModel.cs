﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;
using Prism.Navigation;
using WifiPrison.Base;
using WifiPrison.Interfaces;
using Xamarin.Forms;

namespace WifiPrison.ViewModels
{
    public class WifiPageViewModel : CustomViewModel
    {
        private readonly INavigationService _navigationService;
        private readonly IWifiManager _wifiManager;

        private string _connected ;

        public string WifiConnected
        {
            get { return _connected; }
            set
            {
                SetProperty(ref _connected, value);
            }
        }

        private bool _isconnected;

        public bool IsConnected
        {
            get { return _isconnected; }
            set
            {
                SetProperty(ref _isconnected, value);
            }
        }

        private bool _isRoaming;

        public bool IsRoaming
        {
            get { return _isRoaming; }
            set { SetProperty(ref _isRoaming, value); }
        }

        private string _connectionType;

        public string ConnectionType
        {
            get { return _connectionType; }
            set { SetProperty(ref _connectionType, value); }
        }



        private List<string> _wifiList;

        public List<string> WifiList
        {
            get { return _wifiList; }
            set { SetProperty(ref _wifiList, value); }
        }



        public WifiPageViewModel(IWifiManager wifiManager)
        {
            

            _wifiManager = wifiManager;

            Init();

            _wifiManager.IsWifiConnected();

        }

        private async void Init()
        {
            WifiConnected = "loading ...";

            await Task.Run(() => InitWifiInfo());

            WifiConnected = IsConnected ? "Wifi Connected" : "Wifi Disconnected";
        }

        private async void   InitWifiInfo()
        {
            IsConnected = _wifiManager.IsWifiConnected();
            IsRoaming = _wifiManager.IsRoaming();
            ConnectionType = _wifiManager.GetConnectionType();
        }

        

      
         
     

        
    }
}
