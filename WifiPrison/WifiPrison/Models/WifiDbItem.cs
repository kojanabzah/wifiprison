﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace WifiPrison.Models
{
    public class WifiDbItem
    {
        public WifiDbItem()
        {
                
        }

        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

        public DateTime TimeConnected { get; set; }
    }
}
