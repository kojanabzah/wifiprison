﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiPrison.Models
{
    public class WifiItem
    {
        public string Name { get; set; }

        public int Level { get; set; }

        public int Frequency { get; set; }

        public string Capabilities { get; set; }


        public string SignalLevel { get; set; }




    }
}
