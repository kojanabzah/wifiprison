﻿using System.Threading.Tasks;

namespace WifiPrison.Interfaces
{
    public interface IPopupService
    {
        Task ShowDialog();
    }

}
