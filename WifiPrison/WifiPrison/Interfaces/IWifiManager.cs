﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WifiPrison.Models;

namespace WifiPrison.Interfaces
{
    public interface IWifiManager
    {
        bool IsWifiConnected();
        bool IsWifiEnabled();

        void GetWifiNetworks(EventHandler callback);

        List<WifiItem> GetWifiList();

        bool IsRoaming();

        string GetConnectionType();

        void ConnectWifi(string networkSSID,string password);

        string GetWifiExtraInfo();
    }
}
