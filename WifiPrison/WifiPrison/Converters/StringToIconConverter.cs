﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace WifiPrison.Converters
{
    public class StringToIconConverter :IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                switch (value as string)
                {
                    case "Info":
                        return "info.png";
                        break;
                    case "Wifi List":
                        return "wifilist";
                        break;
                    case "History":
                        return "history.png";
                        break;
                    case "Settings":
                        return "setting.png";
                        break;
                }
            }
            return "yes.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
