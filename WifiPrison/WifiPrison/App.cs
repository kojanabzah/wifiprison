﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WifiPrison.Data;
using WifiPrison.Interfaces;
using Xamarin.Forms;

namespace WifiPrison
{
    public class App : Application
    {
        static WifiItemDatabase database;
        private readonly IWifiManager _wifiManager;
        public App()
        {
            var bs = new Bootstrapper();
            bs.Run(this);
        }

        public static WifiItemDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new WifiItemDatabase();
                }
                return database;
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
